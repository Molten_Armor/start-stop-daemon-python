#!/usr/bin/python3
# coding: utf-8
import time
import signal
import sys

if __name__ == '__main__':
    def handle_term(*args) -> None:
        time.sleep(30)
        sys.exit(0)

    signal.signal(signal.SIGTERM, handle_term)

    while True:
        time.sleep(999)
