#!/bin/python3
# Written on 2023-9-8
# Author: kwok
# /// script
# requires-python = '>=3.10'
# dependencies = [
#   'docopt'
# ]
# ///
#
import docopt
import os
import sys
import time


def _disable_core_dump() -> None:
    '''The function which disables core dumping for better security.'''
    try:
        import resource
        resource.setrlimit(resource.RLIMIT_CORE, (0, 0))
    except OSError:
        print('Rlimit is not supported on this system, starting anyway...')
        return None

def _close_all_fds() -> None:
    '''The function which closes all file descriptors.'''
    if os.path.exists('/proc'):
        for fd in os.listdir('/proc/self/fd'):
            if fd not in ('0', '1', '2'):
                try:
                    os.close(int(fd))
                except OSError:
                    continue

def _detect_exec(_exec: str, _args: list[str]) -> None:
    '''The function that detects the process by its name.
    And if it exists, this program will exit, returning an abnormal status code.
    '''
    if os.path.exists('/proc'):
        for entry in os.listdir('/proc'):
            abs_path: str = os.path.join('/proc', entry)
            if (entry.isdigit()) and (os.path.isdir(abs_path)):
                try:
                    with open('{}/cmdline'.format(abs_path), 'r') as cmdline_fd:
                        cmdline_args: list[str] = cmdline_fd.read().strip('\0').split('\0')[1:]
                        if (os.path.realpath(_exec) == os.readlink('{}/exe'.format(abs_path))) and (set(_args) == set(cmdline_args)):
                            print('There is a similar process running: {}, please use -f option if you insist.'.format(entry))
                            sys.exit(1)
                except (PermissionError, FileNotFoundError):
                    continue
    else:
        print('Procfs is not found on this system, skipping...')

def _detect_pidfile(_pidfile: str) -> None:
    '''The function that detects the process using pidfile.
    And if it exists, this program will exit, returning an abnormal status code.
    '''
    global ORIG_CWD
    if (_pidfile != '/dev/null'):
        if (os.path.realpath(_pidfile) != _pidfile):
            _pidfile = os.path.realpath(os.path.join(ORIG_CWD, _pidfile))
    if os.path.exists(_pidfile):
        with open(_pidfile, 'r') as pidfile_fd:
            pid: int = int(pidfile_fd.read())
        try:
            os.kill(pid, 0)
            print('The process is still running!')
            sys.exit(1)
        except ProcessLookupError:
            print('The process has exited, but the pidfile still exists. Please remove it manually, or start with -f option to override it.')
            sys.exit(1)

def _create_pidfile(_pidfile: str) -> None:
    '''The function that creates pidfile.'''
    global ORIG_CWD
    try:
        if (_pidfile != '/dev/null'):
            if (os.path.realpath(_pidfile) != _pidfile):
                _pidfile = os.path.realpath(os.path.join(ORIG_CWD, _pidfile))
            with open(_pidfile, 'w+') as pidfile_fd:
                pidfile_fd.write(str(os.getpid()))
    except PermissionError:
        print('Can not create PID file. Permission denied!')
        sys.exit(1)

def _redirect_output(_log_file: str) -> None:
    '''The function that redirects output.'''
    global ORIG_CWD
    if (os.path.realpath(_log_file) != _log_file):
        _log_file = os.path.realpath(os.path.join(ORIG_CWD, _log_file))
    log_fd = os.open(_log_file, os.O_RDWR | os.O_CREAT | os.O_TRUNC)
    os.dup2(log_fd, sys.stdout.fileno())
    os.dup2(log_fd, sys.stderr.fileno())

def _clear_env() -> None:
    '''The function that clears environment variables.'''
    os.environ.clear()

def _get_user(_target_user: int | str) -> int:
    import pwd
    try:
        return int(_target_user)
    except ValueError:
        try:
            return pwd.getpwnam(_target_user)[2]    # type: ignore
        except KeyError:
            print('User {} does not exist!'.format(_target_user))
            sys.exit(1)

def _change_user(_target_user) -> None:
    '''The function that changes user.'''
    if (os.geteuid() != 0):
        print('Permission denied! Are you root?')
        sys.exit(1)
    else:
        os.setuid(int(_target_user))
        os.seteuid(int(_target_user))

def _get_grp(_target_grp: int | str) -> int:
    import grp
    try:
        return int(_target_grp)
    except ValueError:
        try:
            return grp.getgrnam(_target_grp)[2]    # type: ignore
        except KeyError:
            print('Group {} does not exist!'.format(_target_grp))
            sys.exit(1)

def _change_grp(_target_grp) -> None:
    '''The function that changes group.'''
    if (os.getegid() != 0):
        print('Permission denied! Are you root?')
        sys.exit(1)
    else:
        os.setgid(int(_target_grp))
        os.setegid(int(_target_grp))

def _change_root(_target_root: str) -> None:
    '''The function that changes root.'''
    try:
        if (os.path.realpath(_target_root) != _target_root):
            print('Please use real path for chroot!')
            sys.exit(1)
        if (_target_root != '/'):
            os.chroot(_target_root)
    except (OSError, PermissionError):
        print('Can not run chroot()! Are you root?')
        sys.exit(1)

def _change_dir(_target_dir: str) -> None:
    '''The function that changes work directory.'''
    global ORIG_CWD
    try:
        if (_target_dir != '$PWD'):
            if (os.path.realpath(_target_dir) != _target_dir):
                _target_dir = os.path.realpath(os.path.join(ORIG_CWD, _target_dir))
            os.chdir(_target_dir)
    except (OSError, PermissionError):
        print('Can not change directory!')
        sys.exit(1)

def _set_umask(_target_mask: int) -> None:
    '''The function that changes umask.'''
    try:
        os.umask(_target_mask)
    except (OSError, PermissionError):
        print('Can not set umask!')
        sys.exit(1)

def _set_nice(_target_nice: str) -> None:
    '''The function that sets nice.'''
    try:
        os.nice(int(_target_nice))
    except (ValueError, TypeError):
        print('Can not set nice! Did you enter a valid nice value?')
        sys.exit(1)

def _daemonize() -> None:
    '''The function that demonizes the process.'''
    try:
        if (os.fork()):
            sys.exit(0)
    except OSError as err:
        print('First forking failed! {}'.format(err))
        sys.exit(1)

    try:
        os.setsid()
    except OSError as err:
        print('Creating session failed! {}'.format(err))
        sys.exit(1)

    try:
        if (os.fork()):
            sys.exit(0)
    except OSError as err:
        print('Second forking failed! {}'.format(err))
        sys.exit(1)

def _execute_prog(_path: str, _args: list[str]):
    '''The main function: execute the program.'''
    global ORIG_CWD
    if (os.path.realpath(_path) != _path):
        _path = os.path.realpath(os.path.join(ORIG_CWD, _path))
    try:
        if (os.access(_path, os.X_OK)):
            os.execv(_path, [os.path.basename(_path)] + _args)
        else:
            print('Program does not exist or is not executable!')
            sys.exit(1)
    except Exception as err:
        print('Execute program failed! {}'.format(err))

def _wait_for_stopped(_pid: int, _timeout: int) -> bool:
    end_time: int = round(time.time() + _timeout)
    while True:
        if (round(time.time()) > end_time):
            return False
        else:
            try:
                os.kill(_pid, 0)
            except ProcessLookupError:
                return True
        # I have no idea how to do this more neatly, sorry...
        # This may cost lots of CPU time...
        time.sleep(0.02)

def _get_procs_exec(_exec: str) -> list[int]:
    exec: str = os.path.realpath(_exec)
    proc_list: list[int] = list()
    if (exec != _exec):
        print('Please input the real path of the program or script interpreter.')
        sys.exit(1)
    if os.path.exists('/proc'):
        for entry in os.listdir('/proc'):
            abs_path: str = os.path.join('/proc', entry)
            if (entry.isdigit()) and (os.path.isdir(abs_path)):
                try:
                    if (os.path.realpath(_exec) == os.readlink('{}/exe'.format(abs_path))):
                        proc_list.append(int(entry))
                except (PermissionError, FileNotFoundError):
                    continue
    else:
        print('Procfs is not supported on this system!')
        sys.exit(1)
    return proc_list

def _get_procs_comm(_name: str) -> list[int]:
    name: str = os.path.basename(_name)
    proc_list: list[int] = list()
    if (name != _name):
        print('Please input the base name of the program or script!')
        sys.exit(1)
    if os.path.exists('/proc'):
        for entry in os.listdir('/proc'):
            abs_path: str = os.path.join('/proc', entry)
            if (entry.isdigit()) and (os.path.isdir(abs_path)):
                try:
                    with open('{}/comm'.format(abs_path), 'r') as comm_fd, open('{}/cmdline'.format(abs_path)) as cmdline_fd:
                        cmdline_list: list = cmdline_fd.read().strip('\n').split('\0')
                        cmdline_comm: str = os.path.basename(cmdline_list[1]) if len(cmdline_list) > 1 else ''
                        if (comm_fd.read().strip('\n') == _name) or (cmdline_comm == _name):
                            proc_list.append(int(entry))
                except (PermissionError, FileNotFoundError):
                    continue
    else:
        print('Procfs is not found on this system!')
        sys.exit(1)
    return proc_list

def _stop_procs(_proc_list: list[int], _signal: int, _timeout: int) -> None:
    '''Stop processes according to /proc/PID/exe or /proc/PID/comm.
    WARNING: This may kill some unexpected processes.
    '''
    global STOPPED
    for proc in _proc_list:
        try:
            os.kill(proc, _signal)
            if (_wait_for_stopped(proc, _timeout)):
                return None
            else:
                print('Sending SIGKILL...')
                os.kill(proc, 9)
                if (_wait_for_stopped(proc, _timeout)):
                    return None
                else:
                    print('Stopping job possibly failed!')
                    STOPPED = False
        except ProcessLookupError:
            continue
        except PermissionError:
            print('Permission denied!')

def _convert_pidfile(_pidfile: str) -> int:
    '''Convert pidfile to pid.'''
    try:
        with open(_pidfile, 'r') as pidfile_fd:
            return int(pidfile_fd.read().strip())
    except PermissionError:
        print('Permission denied!')
        sys.exit(1)
    except FileNotFoundError:
        print('Can not find pidfile!')
        sys.exit(1)

def _stop_proc_pid(_pid: int, _signal: int, _timeout: int) -> None:
    '''Stop the process according to its PID.'''
    global STOPPED
    try:
        os.kill(_pid, _signal)
        if (_wait_for_stopped(_pid, _timeout)):
            return None
        else:
            print('Sending SIGKILL...')
            os.kill(_pid, 9)
            if (_wait_for_stopped(_pid, _timeout)):
                return None
            else:
                print('Stopping job possibly failed!')
                STOPPED = False
    except ProcessLookupError:
        print('Process does not exist!')
    except PermissionError:
        print('Permission denied!')

def _remove_pidfile(_pidfile: str) -> None:
    '''Remove the pidfile.'''
    try:
        os.remove(os.path.realpath(_pidfile))
    except FileNotFoundError:
        return None
    except PermissionError:
        print('Permission denied!')
        sys.exit(1)

def _show_status_procs(_proc_list: list[int]):
    if (len(_proc_list) > 0):
        for proc in _proc_list:
            print('\033[32m{}\033[0m {}: {}'.format('[Running]', 'Process is running', proc))
    else:
        print('\033[31m{}\033[0m {}'.format('[Stopped]', 'Process is not running.'))

def _show_status_pidfile(_pidfile: str) -> None:
    '''Show process's status by finding its pidfile.'''
    try:
        with open(_pidfile, 'r') as pidfile_fd:
            pid: int = int(pidfile_fd.read())
    except FileNotFoundError:
        print('\033[31m{}\033[0m {}'.format('[Failed]', 'Process is not running.'))
        sys.exit(1)

    try:
        os.kill(pid, 0)
        print('\033[32m{}\033[0m {}: {}'.format('[Running]', 'Process is running', pid))
    except ProcessLookupError:
        print('\033[31m{}\033[0m {}'.format('[Stopped]', 'Process is not running.'))

def _show_status_pid(_pid: int) -> None:
    '''Show process's status by its PID.'''
    try:
        os.kill(_pid, 0)
        print('\033[32m{}\033[0m {}: {}'.format('[Running]', 'Process is running', _pid))
    except ProcessLookupError:
        print('\033[31m{}\033[0m {}'.format('[Stopped]', 'Process is not running.'))


if __name__ == '__main__':
    help_str: str = '''
Usage:
    {0} start -x <prog_path> [-b] [-p <pidfile>] [options] [--] [<prog_args>...]
    {0} stop (-x <prog_path> | -n <name> | -p <pidfile> | --pid <pid>) [-t <timeout>] [-s <signal>]
    {0} status (-x <prog_path> | -n <name> | -p <pidfile> | --pid <pid>)
    {0} --help
    {0} --version

General Options:
    -x, --exec <prog_path>    Path of the program to be started
    -p, --pidfile <pidfile>    Absolute path of the pidfile for the program, if the program itself doesn't create a pidfile when started, you can use this option to create one

start Options:
    -b, --background    Detach the process from the terminal, using fork() and setsid()
    -f, --force    Run the program even if its pidfile already exists
    -c, --clear  Clear all environment variables before starting
    -O, --output <file>    Redirect output to a file
    -k, --umask <mask>    Set umask when starting [default: 022]
    -r, --chroot <root>    Chroot when starting, NEEDS ROOT PRIVILEGE [default: /]
    -u, --user <user>    Change user when starting, NEEDS ROOT PRIVILEGE
    -g, --group <group>    Change group when starting, NEEDS ROOT PRIVILEGE
    -d, --chdir <dir>    Chdir when starting, NEEDS ROOT PRIVILEGE [default: $PWD]
    -N, --nice <nice>    Set nice when starting, NEEDS ROOT PRIVILEGE

stop and status Options:
    -t, --timeout <timeout>    Timeout for stopping job [default: 3]
    -s, --signal <signal>    Signal number to send when stopping the process [default: 15]
    -n, --name <name>    Base name of the program, used to find it, this is especially useful when you run a script which needs an interpreter
    --pid <pid>    PID of the process, used to find it
'''.format(sys.argv[0])

    VERSION: str = '0.03'
    global ORIG_CWD
    ORIG_CWD: str = os.getcwd()

    # Get options and positional arguments.
    args = docopt.docopt(help_str, version=VERSION)

    if (args['start'] == True):
        # 1. Do some preparing jobs, for example, disable core dump.
        # 2. Fork twice if the user wants.
        # 3. Create pidfile if the user wants.
        # 4. Do what the user wants.
        if (not args['--force']):
            if (args['--pidfile']):
                _detect_pidfile(args['--pidfile'])
            else:
                _detect_exec(args['--exec'], args['<prog_args>'])

        # Let's detect the program as early as possible for better performance.
        # In fact this detection is not necessary.
        if not (os.access(args['--exec'], os.X_OK)):
            print('Program does not exist or is not executable!')
            sys.exit(1)

        _disable_core_dump()
        os.dup2(os.open('/dev/null', os.O_RDONLY), sys.stdin.fileno())
        _close_all_fds()

        # Demonize the process, if needed.
        if (args['--background']):
            _daemonize()

        if (args['--pidfile']):
            _create_pidfile(args['--pidfile'])


        if (args['--clear']):
            _clear_env()

        if (args['--nice']):
            _set_nice(args['--nice'])

        if (args['--group']):
            new_grp: int = _get_grp(args['--group'])

        if (args['--user']):
            new_user: int = _get_user(args['--user'])

        if (args['--chroot']):
            _change_root(args['--chroot'])

        if (args['--group']):
            _change_grp(new_grp)    # type: ignore

        if (args['--user']):
            _change_user(new_user)    # type: ignore

        if (args['--chdir']):
            _change_dir(args['--chdir'])

        if (args['--umask']):
            _set_umask(int(args['--umask'], 8))

        # Execute the program.
        _execute_prog(args['--exec'], args['<prog_args>'])

    elif (args['stop'] == True):
        # WARNING:
        # If you do not stop the process by its PID File or PID,
        # start-stop-daemon will stop ALL PROCESSES that MATCH THE NAME,
        # just like killall.
        # 1. Send signal to stop the process.
        # 2. Remove the pidfile.
        global STOPPED
        STOPPED: bool = True
        if (args['--exec']):
            _stop_procs(_get_procs_exec(args['--exec']), int(args['--signal']), int(args['--timeout']))
        elif (args['--name']):
            _stop_procs(_get_procs_comm(args['--name']), int(args['--signal']), int(args['--timeout']))
        elif (args['--pidfile']):
            _stop_proc_pid(_convert_pidfile(args['--pidfile']), int(args['--signal']), int(args['--timeout']))
            _remove_pidfile(args['--pidfile'])
        elif (args['--pid']):
            _stop_proc_pid(int(args['--pid']), int(args['--signal']), int(args['--timeout']))

        if (not STOPPED):
            sys.exit(1)

    elif (args['status'] == True):
        # 1. Find the pidfile.
        # 2. If it exists, then the process is running, and print its PID.
        # 3. If it doesn't, then the process is dead, and we will do nothing.
        if (args['--exec']):
            _show_status_procs(_get_procs_exec(args['--exec']))
        elif (args['--name']):
            _show_status_procs(_get_procs_comm(args['--name']))
        elif (args['--pidfile']):
            _show_status_pidfile(args['--pidfile'])
        elif (args['--pid']):
            _show_status_pid(int(args['--pid']))
